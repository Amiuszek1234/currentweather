/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.client.RestTemplate;


/**
 *
 * @author adamz
 */
@Lazy
@Configuration
public class SpringConfiguration {
/*@Bean 
public WriteToDb wtDb()
{
    return new WriteToDb();
}*/
@Bean 
public Db wtDb()
{
    return new Db();
}

}