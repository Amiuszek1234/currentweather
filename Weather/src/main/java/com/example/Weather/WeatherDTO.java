/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;

/**
 *
 * @author adamz
 */
public class WeatherDTO {
   private String city;
   private double temperature;

   WeatherDTO()
   {
   
   }
   WeatherDTO(String city, double temperature)
   {
    this.city = city;
    this.temperature=temperature;
   }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }
   
   
          
    
    
}
