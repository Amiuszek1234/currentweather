/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author adamz
 */

public class Rain {

@SerializedName("1h")
@Expose
private Double _1h;

public Double get1h() {
return _1h;
}

public void set1h(Double _1h) {
this._1h = _1h;
}    
}

