/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author adamz
 */

public class Sys {
    @SerializedName("type")
@Expose
private Integer type;
@SerializedName("id")
@Expose
private Integer id;
@SerializedName("country")
@Expose
private String country;
@SerializedName("sunrise")
@Expose
private Integer sunrise;
@SerializedName("sunset")
@Expose
private Integer sunset;

public Integer getType() {
return type;
}

public void setType(Integer type) {
this.type = type;
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getCountry() {
return country;
}

public void setCountry(String country) {
this.country = country;
}

public Integer getSunrise() {
return sunrise;
}

public void setSunrise(Integer sunrise) {
this.sunrise = sunrise;
}

public Integer getSunset() {
return sunset;
}

public void setSunset(Integer sunset) {
this.sunset = sunset;
}

}
