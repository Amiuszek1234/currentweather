/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.*;
/**
 *
 * @author adamz
 */
@RestController //konocowka Restowa 
public class WeatherRestController {
@Autowired
Db weatherDb;
/*@Autowired
WriteToDb writeToDb;*/
@CrossOrigin
@GetMapping("getWeather")
public String  getWeather()
{
   
    //writeToDb.setSession();
    weatherDb.setSession();
    ObjectMapper mapper = new ObjectMapper();
    try
    {
       // String jsonString = mapper.writeValueAsString(writeToDb.getJsonData());
        String jsonString = mapper.writeValueAsString(weatherDb.getJsonData());
        return jsonString;
    }
    catch (Exception e)
    {
                
    }
    
    return null;
    
}

}
