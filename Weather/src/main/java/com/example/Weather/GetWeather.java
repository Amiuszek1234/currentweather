/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author adamz
 */

public class GetWeather {

    public void get()
    {
        RestTemplate restTemplate = new RestTemplate();
        Scanner scanner = new Scanner (System.in);
        String city = "";
        String stringUrl = "http://api.openweathermap.org/data/2.5/weather?q=";               
        System.out.println("Podaj nazwę miejscowości, dla której pobrać aktualna temperaturę");
        //city = System.console().readLine();
        city = scanner.next();
        //city = "Koszalin";
        stringUrl += city +"&units=metric&appid=1e876d3730dd950066b841dce23365fe";
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
        headers.set("X-Cache-Key", "data/2.5/weather?q=koszalin");
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<WeatherClass> response = restTemplate.exchange(stringUrl, HttpMethod.GET, entity,WeatherClass.class);
        WeatherClass result =response.getBody();
        System.out.println(result.getMain().getTemp());
        Wt wt = new Wt();
        wt.setCity(city);
        wt.setTemperature(result.getMain().getTemp());
        Db db = new Db(wt);
        db.addRecord();
}

}
