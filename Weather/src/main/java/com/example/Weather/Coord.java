/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author adamz
 */

public class Coord {

@SerializedName("lon")
@Expose
private Double lon;
@SerializedName("lat")
@Expose
private Double lat;

public Double getLon() {
return lon;
}

public void setLon(Double lon) {
this.lon = lon;
}

public Double getLat() {
return lat;
}

public void setLat(Double lat) {
this.lat = lat;
}    
}
