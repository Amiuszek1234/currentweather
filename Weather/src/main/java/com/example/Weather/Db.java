/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Weather;

import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author adamz
 */
public class Db {
    private Session session;
    private static Wt weather;
    
    public Db(Wt weather)
    {
      this.weather = weather;
    }
    public Db()
    {
        
    }
    
    public void deleteFromDb()
    {
         String query = "Delete from public.WT";
         Query queryObj = session.createSQLQuery(query);
         int updated = queryObj.executeUpdate();
        
    }
    
    public void setSession()
    {
        HibernateFactory hibernateFactory= new HibernateFactory();
        this.session = hibernateFactory.getSessionFactory().openSession();
    }
    
    public void addRecord()
        {
            this.setSession();
            Transaction transaction = this.session.beginTransaction();
            try {
                deleteFromDb();
                this.session.save(weather);
                this.session.getTransaction().commit();
               // getJsonData();
                } catch (Exception ex) {
                transaction.rollback();
                ex.printStackTrace();
                throw new RuntimeException(ex);
                } finally {
                //session.close();
                }

        }
    
     WeatherDTO getJsonData()
    {
        Wt data=null;
        String strQuery = "from Wt";
        System.out.println(this.session);
         Query query = this.session.createQuery(strQuery);
         Iterator it = query.iterate();
         while (it.hasNext())
         {
             data = (Wt)it.next();
         }
         
         return new WeatherDTO(data.getCity(),data.getTemperature());
        
    }
}
